const merge = require('lodash/merge');
const cssMatcher = require('jest-matcher-css');
const postcss = require('postcss');
const tailwindcss = require('tailwindcss');
const customPlugin = require('./index.js');

expect.extend({
  toMatchCss: cssMatcher,
});

function generatePluginCss(overrides) {
  const config = {
    theme: {
      vectorEffect: {},
    },
    variants: {
      vectorEffect: [],
    },
    corePlugins: false,
    plugins: [customPlugin],
  };

  return postcss(tailwindcss(merge(config, overrides)))
    .process('@tailwind utilities', {
      from: undefined,
    })
    .then(({ css }) => css);
}

test('utility classes can be generated', () => {
  return generatePluginCss().then(css => {
    expect(css).toMatchCss(`
      .non-scaling-stroke {
        vector-effect: non-scaling-stroke
      }

      .non-scaling-size {
        vector-effect: non-scaling-size
      }

      .non-rotation {
        vector-effect: non-rotation
      }

      .fixed-position {
        vector-effect: fixed-position
      }
    `);
  });
});

test('variants can be customized', () => {
  return generatePluginCss({
    theme: {
      screens: {
        sm: '640px',
      },
    },
    variants: {
      vectorEffect: ['responsive', 'hover'],
    },
  }).then(css => {
    expect(css).toMatchCss(`
    .non-scaling-stroke {
      vector-effect: non-scaling-stroke
    }
    .non-scaling-size {
      vector-effect: non-scaling-size
    }
    .non-rotation {
      vector-effect: non-rotation
    }
    .fixed-position {
      vector-effect: fixed-position
    }
    .hover\\:non-scaling-stroke:hover {
      vector-effect: non-scaling-stroke
    }
    .hover\\:non-scaling-size:hover {
      vector-effect: non-scaling-size
    }
    .hover\\:non-rotation:hover {
      vector-effect: non-rotation
    }
    .hover\\:fixed-position:hover {
      vector-effect: fixed-position
    }
    @media (min-width: 640px) {
      .sm\\:non-scaling-stroke {
        vector-effect: non-scaling-stroke
      }
      .sm\\:non-scaling-size {
        vector-effect: non-scaling-size
      }
      .sm\\:non-rotation {
        vector-effect: non-rotation
      }
      .sm\\:fixed-position {
        vector-effect: fixed-position
      }
      .sm\\:hover\\:non-scaling-stroke:hover {
        vector-effect: non-scaling-stroke
      }
      .sm\\:hover\\:non-scaling-size:hover {
        vector-effect: non-scaling-size
      }
      .sm\\:hover\\:non-rotation:hover {
        vector-effect: non-rotation
      }
      .sm\\:hover\\:fixed-position:hover {
        vector-effect: fixed-position
      }
    }
    `);
  });
});
