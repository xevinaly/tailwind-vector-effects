# Tailwind Vector Effect

> Adds Vector Effects to tailwind for the svg path tag

Install the plugin from npm:

```
$ npm install tailwind-vector-effect
```

Then add the plugin to your `tailwind.config.js` file:

```js
// tailwind.config.js
module.exports = {
  // ...
  plugins: [
    // ...
    require('tailwind-vector-effect'),
    // ...
  ],
  // ...
};
```

This plugin will generate following CSS:

```css
.non-scaling-stroke {
  vector-effect: non-scaling-stroke;
}

.non-scaling-size {
  vector-effect: non-scaling-size;
}

.non-rotation {
  vector-effect: non-rotation;
}

.fixed-position {
  vector-effect: fixed-position;
}
```

## License

Tailwind Vector Effect is licensed under the MIT License.

## Credits

Created with [create-tailwind-plugin](https://github.com/Landish/create-tailwind-plugin).
