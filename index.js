const plugin = require('tailwindcss/plugin');

module.exports = plugin(
  function ({ addUtilities, variants }) {
    const utilities = {
      '.non-scaling-stroke': {
        'vector-effect': 'non-scaling-stroke',
      },
      '.non-scaling-size': {
        'vector-effect': 'non-scaling-size',
      },
      '.non-rotation': {
        'vector-effect': 'non-rotation',
      },
      '.fixed-position': {
        'vector-effect': 'fixed-position',
      },
    };

    addUtilities(utilities, {
      variants: variants('vectorEffect'),
    });
  },
  {
    theme: {
      vectorEffect: {},
    },
    variants: {
      vectorEffect: [],
    },
  }
);
